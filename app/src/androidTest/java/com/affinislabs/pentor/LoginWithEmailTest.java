package com.affinislabs.pentor;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.affinislabs.pentor.ui.activities.AuthActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LoginWithEmailTest {

    private final String TEST_EMAIL = "test2@intellectualapps.com";
    private final String TEST_PASSWORD = "password";

    @Rule
    public ActivityTestRule<AuthActivity> mActivityRule = new ActivityTestRule<>(
            AuthActivity.class);

    @Test
    public void loginWithEmail() throws Exception {
        onView(withId(R.id.email_address)).perform(clearText());
        onView(withId(R.id.email_address)).perform(typeText(TEST_EMAIL));
        closeSoftKeyboard();
        onView(withId(R.id.password)).perform(clearText());
        onView(withId(R.id.password)).perform(typeText(TEST_PASSWORD));
        closeSoftKeyboard();
        onView(withId(R.id.login_button)).perform(click());

        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
    }
}
