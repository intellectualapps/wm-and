package com.affinislabs.pentor;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.affinislabs.pentor.utils.DeviceUuidFactory;
import com.affinislabs.pentor.utils.FirebaseUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.database.FirebaseDatabase;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.File;
import io.fabric.sdk.android.Fabric;

public class WelcomeMatApplication extends Application {

    static File cacheFile;
    private static WelcomeMatApplication welcomeMatApplication;

    public static WelcomeMatApplication get(Context context) {
        return (WelcomeMatApplication) context.getApplicationContext();
    }

    public static Resources getApplicationResources() {
        return getAppInstance().getResources();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        cacheFile = new File(getCacheDir(), "responses");
        welcomeMatApplication = this;

        final TwitterAuthConfig authConfig = new TwitterAuthConfig(getString(R.string.twitter_key), getString(R.string.twitter_secret));
        Fabric.with(this, new Twitter(authConfig), new Crashlytics());

        DeviceUuidFactory deviceIdFactory = new DeviceUuidFactory(this);
        PreferenceStorageManager.saveDeviceId(getApplicationContext(), deviceIdFactory.getStringUUID());

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseUtils.initializeFirebase();
    }

    public static File getCacheFile() {
        return cacheFile;
    }

    public static WelcomeMatApplication getAppInstance() {
        if (welcomeMatApplication == null) {
            welcomeMatApplication = new WelcomeMatApplication();
        }
        return welcomeMatApplication;
    }
}
