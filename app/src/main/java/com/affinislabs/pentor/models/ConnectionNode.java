package com.affinislabs.pentor.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.affinislabs.pentor.utils.Constants;
import com.google.android.gms.common.api.BooleanResult;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

public class ConnectionNode implements Parcelable {
    @SerializedName("id")
    @Expose
    private String connectionId;
    @SerializedName("participants")
    @Expose
    private Map<String, Object> participantData;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;

    public ConnectionNode(String connectionId) {
        this.connectionId = connectionId;
    }

    private ConnectionNode(Parcel in) {
        connectionId = in.readString();
        currentStatus = in.readString();
        participantData = (Map<String, Object>) in.readSerializable();
    }

    public static final Creator<ConnectionNode> CREATOR = new Creator<ConnectionNode>() {
        @Override
        public ConnectionNode createFromParcel(Parcel in) {
            return new ConnectionNode(in);
        }

        @Override
        public ConnectionNode[] newArray(int size) {
            return new ConnectionNode[size];
        }
    };

    public ConnectionNode() {

    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Map<String, Object> getParticipantData() {
        return participantData;
    }

    public void setParticipants(Map<String, Object> participantData) {
        this.participantData = participantData;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(connectionId);
        dest.writeString(currentStatus);
        dest.writeSerializable((Serializable) participantData);
    }
}
