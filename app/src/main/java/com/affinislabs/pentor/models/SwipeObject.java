package com.affinislabs.pentor.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;

@NonReusable
@Layout(R.layout.mentor_item_layout)
public class SwipeObject {
    private static int count;
    @View(R.id.mentor_profile_photo)
    private ImageView mentorProfilePhoto;

    @View(R.id.mentor_name)
    private TextView mentorName;

    @View(R.id.mentor_short_bio)
    private TextView mentorShortBio;

    private User user;

    private PentorInterfaces.MentorClickListener mentorClickListener;
    private Context context;


    public SwipeObject(User user, PentorInterfaces.MentorClickListener mentorClickListener, Context context) {
        this.user = user;
        this.mentorClickListener = mentorClickListener;
        this.context = context;
    }

    @Click(R.id.mentor_profile_photo)
    private void onClick() {

    }

    @Resolve
    private void onResolve() {
        mentorName.setText(user.getFullName());
        mentorShortBio.setText(user.getShortBio());
        if (user.getProfilePhotoUrl() != null) {
            if (user.getProfilePhotoUrl().contains(context.getString(R.string.base_url))) {
                String imageKey = user.getProfilePhotoUrl().split("image/")[1];

                if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                    String imageData = FileUtils.getImageDataFromCache(imageKey);
                    try {
                        byte[] decodedImageResource = Base64.decode(imageData, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                        mentorProfilePhoto.setImageBitmap(decodedByte);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    fetchImageBackgroundTask(imageKey, mentorProfilePhoto, context);
                }
            } else {
                GlideUtils.loadGlideImage(mentorProfilePhoto, context, user.getProfilePhotoUrl());
            }
        } else {
            mentorProfilePhoto.setImageDrawable(null);
        }
    }

    @SwipeOut
    private void onSwipedOut() {

        mentorClickListener.onMentorSkipClicked(user, count);
    }

    @SwipeCancelState
    private void onSwipeCancelState() {

    }

    @SwipeIn
    private void onSwipeIn() {

        mentorClickListener.onMentorAddClicked(user, count);
    }

    private void fetchImageBackgroundTask(final String imageKey, final ImageView profilePhoto, final Context context) {
        if (NetworkUtils.isConnected(context)) {
            new ApiClient.NetworkCallsRunner(Constants.FETCH_IMAGE_REQUEST, imageKey, new ApiClientListener.FetchEncodedImageListener() {
                @Override
                public void onEncodedImageFetched(ImageResponse imageResponse) {
                    if (imageResponse != null && imageResponse.getBase64ImageString() != null && imageResponse.getBase64ImageString().length() > 0) {
                        FileUtils.saveImageToCache(imageKey, imageResponse.getBase64ImageString());
                        byte[] decodedImageResource = Base64.decode(imageResponse.getBase64ImageString(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                        profilePhoto.setImageBitmap(decodedByte);
                    }
                }
            }).execute();
        }
    }

    @SwipeInState
    private void onSwipeInState() {

    }

    @SwipeOutState
    private void onSwipeOutState() {

    }
}
