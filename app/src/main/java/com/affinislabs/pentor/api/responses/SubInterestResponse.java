package com.affinislabs.pentor.api.responses;

import com.affinislabs.pentor.models.SubInterest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Thundeyy on 01/02/2017.
 */
public class SubInterestResponse {
    @SerializedName("subInterests")
    @Expose
    private ArrayList<SubInterest> subInterests = null;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("developerMessage")
    @Expose
    private String developerMessage;

    public ArrayList<SubInterest> getSubInterests() {
        return subInterests;
    }

    public void setSubInterests(ArrayList<SubInterest> subInterests) {
        this.subInterests = subInterests;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }
}
