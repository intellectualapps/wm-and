package com.affinislabs.pentor.api;

import com.affinislabs.pentor.api.responses.ConnectionResponse;
import com.affinislabs.pentor.api.responses.FCMTokenResponse;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.api.responses.InterestResponse;
import com.affinislabs.pentor.api.responses.LoginResponse;
import com.affinislabs.pentor.api.responses.MentorConnectionResponse;
import com.affinislabs.pentor.api.responses.MentorResponse;
import com.affinislabs.pentor.api.responses.ProfileUpdateResponse;
import com.affinislabs.pentor.api.responses.RegistrationResponse;
import com.affinislabs.pentor.api.responses.SubInterestResponse;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.Connection;

public class ApiClientListener {
    public interface AccountRegistrationListener {
        void onAccountRegistered(RegistrationResponse registrationResponse);
    }

    public interface AccountAuthenticationListener {
        void onUserAuthenticated(LoginResponse loginResponse);
    }

    public interface FetchUserProfileListener {
        void onProfileFetched(UserProfileResponse userProfileResponse);
    }

    public interface ResetPasswordListener {
        void onPasswordReset(UserProfileResponse userProfileResponse);
    }

    public interface ProfileUpdatedListener {
        void onProfileUpdated(ProfileUpdateResponse profileUpdateResponse);
    }

    public interface FetchInterestsListener {
        void onInterestsFetched(InterestResponse interestResponse);
    }

    public interface FetchSubInterestsListener {
        void onSubInterestsFetched(SubInterestResponse subInterestResponse);
    }

    public interface FetchMentorsListener {
        void onMentorsFetched(MentorResponse mentorResponse);
    }

    public interface FetchConnectionsListener {
        void onConnectionsFetched(ConnectionResponse connectionResponse);
    }

    public interface UpdateConnectionStatusListener {
        void onStatusUpdated(Connection connectionResponse);
    }

    public interface MentorConnectionListener {
        void onConnectionEstablished(MentorConnectionResponse mentorConnectionResponse);
    }

    public interface FetchEncodedImageListener {
        void onEncodedImageFetched(ImageResponse imageResponse);
    }

    public interface UploadFCMTokenListener {
        void onFCMTokenUploaded(FCMTokenResponse fcmTokenResponse);
    }

    public interface RateUserListener {
        void onUserRated(FCMTokenResponse fcmTokenResponse);
    }

    public interface ReportUserListener {
        void onUserReported();
    }

    public interface BlockUserListener {
        void onUserBlocked();
    }
}
