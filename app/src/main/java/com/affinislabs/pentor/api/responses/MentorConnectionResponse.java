package com.affinislabs.pentor.api.responses;

import com.affinislabs.pentor.models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MentorConnectionResponse {
    @SerializedName("connectionId")
    @Expose
    private String connectionId;
    @SerializedName("subInterestId")
    @Expose
    private String subInterestId;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("mentor")
    @Expose
    private User mentor;
    @SerializedName("mentee")
    @Expose
    private User mentee;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("developerMessage")
    @Expose
    private String developerMessage;

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getSubInterestId() {
        return subInterestId;
    }

    public void setSubInterestId(String subInterestId) {
        this.subInterestId = subInterestId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public User getMentor() {
        return mentor;
    }

    public void setMentor(User mentor) {
        this.mentor = mentor;
    }

    public User getMentee() {
        return mentee;
    }

    public void setMentee(User mentee) {
        this.mentee = mentee;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }
}
