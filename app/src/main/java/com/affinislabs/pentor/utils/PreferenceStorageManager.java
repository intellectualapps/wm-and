package com.affinislabs.pentor.utils;

import android.content.Context;

import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PreferenceStorageManager {
    private static final String PREFS_FCM_TOKEN = "prefs_fcm_token";
    private static final String PREFS_AUTH_TOKEN = "prefs_auth_token";

    private static final String PREFS_DEVICE_ID = "prefs_device_id";
    private static final String PREFS_FIRST_NAME = "prefs_first_name";
    private static final String PREFS_GENDER = "prefs_gender";
    private static final String PREFS_LAST_NAME = "prefs_last_name";
    private static final String PREFS_PROFILE_PHOTO = "prefs_profile_photo";
    private static final String PREFS_EMAIL_ADDRESS = "prefs_email_address";
    private static final String PREFS_PASSWORD = "prefs_password";
    private static final String PREFS_USER = "prefs_user";
    private static final String PREFS_CONNECTIONS = "prefs_connections";
    private static final String PREFS_PHOTO_BASE64 = "prefs_photo_base64";
    private static final String PREFS_USER_ID = "prefs_user_id";
    private static final String PREFS_USER_SIGNED_IN = "prefs_signed_in";
    private static final String PREFS_HAS_LOGGED_IN = "prefs_has_logged_in";
    private static final String PREFS_HAS_UPDATED_PROFILE = "prefs_has_updated_profile";

    public static void resetUser(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_USER);
        SharedPrefsUtils.clearPreference(context, PREFS_USER_SIGNED_IN);
        SharedPrefsUtils.clearPreference(context, PREFS_USER_ID);
        SharedPrefsUtils.clearPreference(context, PREFS_AUTH_TOKEN);
        SharedPrefsUtils.clearPreference(context, PREFS_PHOTO_BASE64);
        SharedPrefsUtils.clearPreference(context, PREFS_CONNECTIONS);
        clearSocialData(context);
    }

    public static void clearSocialData(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_GENDER);
        SharedPrefsUtils.clearPreference(context, PREFS_FIRST_NAME);
        SharedPrefsUtils.clearPreference(context, PREFS_LAST_NAME);
        SharedPrefsUtils.clearPreference(context, PREFS_PROFILE_PHOTO);
    }

    public static void saveDeviceId(Context context, String deviceId) {
        SharedPrefsUtils.setStringPreference(context, PREFS_DEVICE_ID, deviceId);
    }

    public static void saveAuthToken(Context context, String apiToken) {
        SharedPrefsUtils.setStringPreference(context, PREFS_AUTH_TOKEN, apiToken);
    }

    public static void saveFCMToken(Context context, String FCMToken) {
        SharedPrefsUtils.setStringPreference(context, PREFS_FCM_TOKEN, FCMToken);
    }

    public static void saveUserId(Context context, String userId) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER_ID, userId);
    }

    public static void saveBase64Photo(Context context, String base64String) {
        SharedPrefsUtils.setStringPreference(context, PREFS_PHOTO_BASE64, base64String);
    }

    public static void saveUser(Context context, User user) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER, StringUtils.userToString(user));
    }


    public static void setSignInStatus(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_USER_SIGNED_IN, status);
    }

    public static void setLoggedInStatus(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_HAS_LOGGED_IN, status);
    }

    public static void setProfileUpdateStatus(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_HAS_UPDATED_PROFILE, status);
    }

    public static void setLoginData(Context context, String emailAddress, String password) {
        SharedPrefsUtils.setStringPreference(context, PREFS_EMAIL_ADDRESS, emailAddress);
        SharedPrefsUtils.setStringPreference(context, PREFS_PASSWORD, password);
    }

    public static void saveSocialProfileData(Context context, String firstName, String lastName, String profilePhoto, String gender) {
        SharedPrefsUtils.setStringPreference(context, PREFS_LAST_NAME, lastName);
        SharedPrefsUtils.setStringPreference(context, PREFS_FIRST_NAME, firstName);
        SharedPrefsUtils.setStringPreference(context, PREFS_GENDER, gender);
        SharedPrefsUtils.setStringPreference(context, PREFS_PROFILE_PHOTO, profilePhoto);
    }

    public static void saveConnections(Context context, ArrayList<Connection> connections) {
        SharedPrefsUtils.setStringPreference(context, PREFS_CONNECTIONS, StringUtils.connectionsToString(connections));
    }


    /**************************************************
     *
     *
     **************************************************/


    public static ArrayList<Connection> getConnections(Context context) {
        return StringUtils.connectionsFromString(SharedPrefsUtils.getStringPreference(context, PREFS_CONNECTIONS));
    }

    public static User getUser(Context context) {
        User user;
        user = StringUtils.userFromString(SharedPrefsUtils.getStringPreference(context, PREFS_USER));
        return user;
    }

    public static String getUserId(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_USER_ID);
    }

    public static String getBase64Photo(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_PHOTO_BASE64);
    }

    public static String getAuthToken(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_AUTH_TOKEN);
    }

    public static String getFCMToken(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_FCM_TOKEN);
    }

    public static String getDeviceId(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_DEVICE_ID);
    }

    public static Map<String, String> getLoginData(Context context) {
        Map<String, String> loginDataMap = new HashMap<String, String>();
        loginDataMap.put(Constants.EMAIL_ADDRESS, SharedPrefsUtils.getStringPreference(context, PREFS_EMAIL_ADDRESS));
        loginDataMap.put(Constants.PASSWORD, SharedPrefsUtils.getStringPreference(context, PREFS_PASSWORD));
        return loginDataMap;
    }

    public static Map<String, String> getSocialProfileData(Context context) {
        Map<String, String> socialDataMap = new HashMap<String, String>();
        socialDataMap.put(Constants.LAST_NAME, SharedPrefsUtils.getStringPreference(context, PREFS_LAST_NAME));
        socialDataMap.put(Constants.FIRST_NAME, SharedPrefsUtils.getStringPreference(context, PREFS_FIRST_NAME));
        socialDataMap.put(Constants.PROFILE_PHOTO, SharedPrefsUtils.getStringPreference(context, PREFS_PROFILE_PHOTO));
        socialDataMap.put(Constants.GENDER, SharedPrefsUtils.getStringPreference(context, PREFS_GENDER));
        return socialDataMap;
    }

    public static boolean getSignInStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_USER_SIGNED_IN, false);
    }

    public static boolean getLoggedInStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_LOGGED_IN, false);
    }

    public static boolean getProfileUpdateStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_UPDATED_PROFILE, false);
    }
}
