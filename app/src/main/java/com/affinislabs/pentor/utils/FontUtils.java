package com.affinislabs.pentor.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class FontUtils {
    public static String FONT_LIGHT = "AvenirLTStd-Light.otf";
    public static String FONT_BLACK = "AvenirLTStd-Black.otf";
    public static String FONT_BOOK = "AvenirLTStd-Book.otf";
    public static String FONT_REGULAR = "AvenirLTStd-Book.otf";
    public static String FONT_BOOK_OBLIQUE = "AvenirLTStd-BookOblique.otf";
    public static String FONT_BLACK_OBLIQUE = "AvenirLTStd-BlackOblique.otf";
    public static String FONT_LIGHT_OBLIQUE = "AvenirLTStd-LightOblique.otf";
    public static String FONT_HEAVY = "AvenirLTStd-Heavy.otf";
    public static String FONT_HEAVY_OBLIQUE = "AvenirLTStd-HeavyOblique.otf";
    public static String FONT_MEDIUM = "AvenirLTStd-Medium.otf";
    public static String FONT_MEDIUM_OBLIQUE = "AvenirLTStd-MediumOblique.otf";
    public static String FONT_OBLIQUE = "AvenirLTStd-Oblique.otf";
    public static String FONT_ROMAN = "AvenirLTStd-Roman.otf";

    public static int STYLE_LIGHT = 3;
    public static int STYLE_REGULAR = 0;
    public static int STYLE_BOLD = 1;

    private static Map<String, Typeface> sCachedFonts = new HashMap<String, Typeface>();

    public static Typeface getTypeface(Context context, String assetPath) {
        if (!sCachedFonts.containsKey(assetPath)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), assetPath);
            sCachedFonts.put(assetPath, tf);
        }
        return sCachedFonts.get(assetPath);
    }

    public static Typeface selectTypeface(Context context, int textStyle) {
        String AvenirPrefix = "fonts/Avenir/";
        String font;
        switch (textStyle) {
            case 0:
                font = FontUtils.FONT_REGULAR;
                break;
            case 1:
                font = FontUtils.FONT_HEAVY;
                break;
            case 2:
                font = FontUtils.FONT_OBLIQUE;
                break;
            case 3:
                font = FontUtils.FONT_LIGHT;
                break;
            case 4:
                font = FontUtils.FONT_MEDIUM;
                break;
            case 5:
                font = FontUtils.FONT_BLACK;
                break;
            case 6:
                font = FontUtils.FONT_ROMAN;
                break;
            case 7:
                font = FontUtils.FONT_BLACK_OBLIQUE;
                break;
            case 8:
                font = FontUtils.FONT_HEAVY_OBLIQUE;
                break;
            case 9:
                font = FontUtils.FONT_LIGHT_OBLIQUE;
                break;
            case 10:
                font = FontUtils.FONT_MEDIUM_OBLIQUE;
                break;
            case 11:
                font = FontUtils.FONT_BOOK_OBLIQUE;
                break;
            default:
                font = FontUtils.FONT_BOOK;
                break;
        }

        return FontUtils.getTypeface(context, AvenirPrefix + font);
    }
}
