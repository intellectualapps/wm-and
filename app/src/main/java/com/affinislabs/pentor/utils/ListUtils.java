package com.affinislabs.pentor.utils;

import com.affinislabs.pentor.models.SubInterest;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Thundeyy on 01/02/2017.
 */

public class ListUtils {
    public static boolean isEmpty(Collection items) {
        return items == null ? true : items.size() < 1;
    }

    public static String[] getSubInterests(ArrayList<SubInterest> subInterests) {
        ArrayList<String> subInterestsArray = new ArrayList<>();
        for (SubInterest subInterest : subInterests) {
            subInterestsArray.add(subInterest.getName());
        }

        return subInterestsArray.toArray(new String[subInterests.size()]);
    }
}
