package com.affinislabs.pentor.ui.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.affinislabs.pentor.WelcomeMatApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import static com.affinislabs.pentor.utils.Constants.WM_GIF_URL;
import com.crashlytics.android.Crashlytics;

public class LaunchActivity extends BaseActivity {
    private static final int LAUNCH_DURATION = 5500;
    private static final String TAG = LaunchActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_launch);
        ImageView gifLoader = (ImageView) findViewById(R.id.gif_loader);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifLoader);
        Glide.with(this).load(WM_GIF_URL).into(imageViewTarget);
    }

    @Override
    protected void onResume() {
        super.onResume();

        new LaunchTask().execute();
    }

    /**
     * This class basically handles the Launch.
     * Edit this class, if you need to perform more actions while doing the launch.
     */
    private class LaunchTask extends AsyncTask {
        Intent intent;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                Thread.sleep(LAUNCH_DURATION);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            User user = PreferenceStorageManager.getUser(WelcomeMatApplication.getAppInstance().getApplicationContext());
            Boolean signedInFlag = PreferenceStorageManager.getSignInStatus(getApplicationContext());

            if (signedInFlag && user != null && user.getEmail() != null && user.getEmail().trim().length() > 0) {
                intent = new Intent(LaunchActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.USER, user);
                Crashlytics.setUserIdentifier(user.getEmail());
                Crashlytics.setUserName(user.getFirstName());
                startActivity(intent);
                finish();
            } else {
                intent = new Intent(LaunchActivity.this, TempActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    }
}
