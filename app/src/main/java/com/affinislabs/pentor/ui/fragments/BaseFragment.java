package com.affinislabs.pentor.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;

import com.affinislabs.pentor.WelcomeMatApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.FCMTokenResponse;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.utils.AppUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.affinislabs.pentor.utils.Validator;

import java.util.Map;

/**
 * Base Fragment to be used by all other fragments
 * <p>
 */
public class BaseFragment extends Fragment {
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    public WelcomeMatApplication getApplication() {
        if (getActivity() != null) {
            return (WelcomeMatApplication) getActivity().getApplication();
        } else {
            return null;
        }
    }

    public void saveSocialProfileData(String firstName, String lastName, String profilePhoto, String gender) {
        PreferenceStorageManager.saveSocialProfileData(WelcomeMatApplication.getAppInstance().getApplicationContext(), firstName, lastName, profilePhoto, gender);
    }

    public void closeFragment() {
        if (getActivity() != null) {
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                hideKeyboard();
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                hideKeyboard();
                getActivity().finish();
            }
        }
    }

    public void showLoadingIndicator(boolean showProgressLoader, String message) {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
        }

        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        if (isAdded()) {
            //progressDialog.setTitle(WelcomeMatApplication.getApplicationResources().getString(R.string.app_name));
        }

        if (showProgressLoader) {
            progressDialog.show();
        } else {
            if (progressDialog.isShowing() && progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }

    public void animateSwipeRefreshLayout(final SwipeRefreshLayout mSwipeRefreshLayout) {
        mSwipeRefreshLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mSwipeRefreshLayout
                        .getViewTreeObserver()
                        .removeOnGlobalLayoutListener(this);
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    public void startFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    private void clearBackStack() {
        FragmentManager manager = getFragmentManager();
        FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
        manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void startFragment(Fragment frag, boolean shouldClearBackstack) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            if (shouldClearBackstack) {
                clearBackStack();
            }
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, backStateName);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commit();
        }
    }

    public void uploadFCMToken(String emailAddress, Map<String, String> metaDataMap) {
        new ApiClient.NetworkCallsRunner(Constants.UPLOAD_FCM_TOKEN_REQUEST, emailAddress, metaDataMap, new ApiClientListener.UploadFCMTokenListener() {
            @Override
            public void onFCMTokenUploaded(FCMTokenResponse fcmTokenResponse) {
            }
        }).execute();
    }

    public boolean validateFields(EditText[] views) {
        return Validator.validateInputViewsNotEmpty(views);
    }

    public boolean validateInputLength(EditText editText, int length) {
        return Validator.validateInputLength(editText, length);
    }

    public boolean validatePhoneNumber(EditText view) {
        return Validator.validatePhoneNumber(view);
    }

    public void fetchImageBackgroundTask(final String imageKey, final ImageView profilePhoto, final Context context) {
        if (NetworkUtils.isConnected(context)) {
            new ApiClient.NetworkCallsRunner(Constants.FETCH_IMAGE_REQUEST, imageKey, new ApiClientListener.FetchEncodedImageListener() {
                @Override
                public void onEncodedImageFetched(ImageResponse imageResponse) {
                    if (imageResponse != null && imageResponse.getBase64ImageString() != null && imageResponse.getBase64ImageString().length() > 0) {
                        FileUtils.saveImageToCache(imageKey, imageResponse.getBase64ImageString());
                        byte[] decodedImageResource = Base64.decode(imageResponse.getBase64ImageString(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                        profilePhoto.setImageBitmap(decodedByte);
                    }
                }
            }).execute();
        }
    }

    public void loadImageData(String imageKey, ImageView imageView) {
        String imageData = FileUtils.getImageDataFromCache(imageKey);
        try {
            byte[] decodedImageResource = Base64.decode(imageData, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
            imageView.setImageBitmap(decodedByte);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User extractUserDetails(UserProfileResponse userProfileResponse) {
        User user = new User();
        user.setEmail(userProfileResponse.getEmail());
        user.setFirstName(userProfileResponse.getFirstName());
        user.setLastName(userProfileResponse.getLastName());
        user.setProfilePhotoUrl(userProfileResponse.getProfilePhotoUrl());
        user.setShortBio(userProfileResponse.getShortBio());
        user.setLongBio(userProfileResponse.getLongBio());
        user.setEmailAddressVerified(userProfileResponse.isEmailAddressVerified());
        user.setPhoneNumber(userProfileResponse.getPhoneNumber());
        user.setGender(userProfileResponse.getGender());
        user.setBirthday(userProfileResponse.getBirthday());
        user.setMentor(userProfileResponse.isMentor());
        user.setLongitude(userProfileResponse.getLongitude());
        user.setLatitude(userProfileResponse.getLatitude());
        user.setAddress(userProfileResponse.getAddress());
        user.setAuthToken(userProfileResponse.getAuthToken());
        user.setUserRoles(userProfileResponse.getUserRoles());
        user.setMentorInterests(userProfileResponse.getMentorInterests());
        return user;
    }

    protected void hideKeyboard() {
        AppUtils.hideKeyboard(getActivity());
    }
}
