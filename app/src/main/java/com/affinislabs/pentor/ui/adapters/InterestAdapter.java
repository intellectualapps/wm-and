package com.affinislabs.pentor.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.models.Interest;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.ListUtils;

import java.util.ArrayList;

public class InterestAdapter extends BaseRecyclerAdapter {
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mInterestImage;
        TextView mInterestLabel;
        public ClickListener clickListener;

        public ViewHolder(View view, ViewHolder.ClickListener clickListener) {
            super(view);
            mInterestLabel = (TextView) view.findViewById(R.id.interest_label);
            mInterestImage = (ImageView) view.findViewById(R.id.interest_image);
            this.clickListener = clickListener;
            view.setOnClickListener(this);
            mInterestLabel.setOnClickListener(this);
            mInterestImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClicked(getAdapterPosition());
            }
        }

        public interface ClickListener {
            void onItemClicked(int position);
        }
    }

    private ArrayList<Interest> mInterests;
    ViewHolder.ClickListener clickListener;

    public InterestAdapter(Context context, ViewHolder.ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    public void setItems(ArrayList<Interest> interests) {
        this.mInterests = interests;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (this.mInterests != null)
            mInterests.clear();
        notifyDataSetChanged();
    }

    public void addItem(Interest interest) {
        if (interest != null) {
            this.mInterests.add(interest);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.mentor_interest_layout, parent, false);

        return new ViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final Interest interest = mInterests.get(position);

        final ViewHolder holder = (ViewHolder) viewHolder;
        if (interest != null) {
            holder.mInterestLabel.setText(interest.getName());
            GlideUtils.loadGlideImage(holder.mInterestImage, context, interest.getImageLink());
        }
    }

    @Override
    public int getItemCount() {
        int count = ListUtils.isEmpty(mInterests) ? 0 : mInterests.size();
        return count;
    }
}
