package com.affinislabs.pentor.ui.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;

import com.affinislabs.pentor.WelcomeMatApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.ProfileActivity;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.DateUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class BasicProfileFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {
    private static final String TAG = BasicProfileFragment.class.getSimpleName();
    Toolbar toolbar;
    View mSnackBarView;
    User user;
    EditText mFirstNameEditText, mLastNameEditText, mPhoneNumberEditText, mGenderEditText, mBirthDayEditText;
    TextView mShowBioView;
    SwitchCompat mentorSwitch;
    Map<String, String> socialProfile = new HashMap<String, String>();
    private Map<String, String> basicProfileData = new HashMap<String, String>();

    ProgressDialog progressDialog;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new BasicProfileFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public BasicProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }

        socialProfile = PreferenceStorageManager.getSocialProfileData(WelcomeMatApplication.getAppInstance().getApplicationContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_basic, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadSocialProfile() {
        if (socialProfile != null && socialProfile.size() > 0 && !PreferenceStorageManager.getProfileUpdateStatus(WelcomeMatApplication.getAppInstance().getApplicationContext())) {

            if (socialProfile.containsKey(Constants.FIRST_NAME) && socialProfile.get(Constants.FIRST_NAME) != null) {
                mFirstNameEditText.setText(socialProfile.get(Constants.FIRST_NAME));
            }
            if (socialProfile.containsKey(Constants.LAST_NAME) && socialProfile.get(Constants.LAST_NAME) != null) {
                mLastNameEditText.setText(socialProfile.get(Constants.LAST_NAME));
            }

            if (socialProfile.containsKey(Constants.GENDER) && socialProfile.get(Constants.GENDER) != null) {
                mGenderEditText.setText((socialProfile.get(Constants.GENDER).equalsIgnoreCase("m") ? "Male" : "Female"));
            }
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((ProfileActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.basic_details_view_label));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        mShowBioView = (TextView) view.findViewById(R.id.show_bio_button);
        mFirstNameEditText = (EditText) view.findViewById(R.id.first_name);
        mLastNameEditText = (EditText) view.findViewById(R.id.last_name);
        mPhoneNumberEditText = (EditText) view.findViewById(R.id.phone_number);
        mGenderEditText = (EditText) view.findViewById(R.id.gender);
        mBirthDayEditText = (EditText) view.findViewById(R.id.birthday);
        mentorSwitch = (SwitchCompat) view.findViewById(R.id.mentor_toggle);

        mShowBioView.setOnClickListener(this);
        mBirthDayEditText.setOnClickListener(this);
        mGenderEditText.setOnClickListener(this);
        mBirthDayEditText.setOnFocusChangeListener(this);
        mGenderEditText.setOnFocusChangeListener(this);
        loadSocialProfile();
        loadUserData(user);

        fetchUserProfile(user.getEmail().trim());
    }

    private void loadUserData(User user) {
        if (user != null) {
            if (user.getFirstName() != null && !(mFirstNameEditText.getText().length() > 0)) {
                mFirstNameEditText.setText(user.getFirstName());
            }
            if (user.getLastName() != null && !(mLastNameEditText.getText().length() > 0)) {
                mLastNameEditText.setText(user.getLastName());
            }
            if (user.getGender() != null) {
                mGenderEditText.setText((user.getGender().equalsIgnoreCase("MALE") ? "Male" : "Female"));
            }

            if (user.getPhoneNumber() != null) {
                mPhoneNumberEditText.setText(user.getPhoneNumber());
            }

            if (user.getBirthday() != null && !user.getBirthday().equalsIgnoreCase("")) {
                String inputString = user.getBirthday().replace("T", " ").replace("Z", "");
                ;
                String format = "yyyy-MM-dd HH:mm:ss";
                DateFormat dateFormat = new SimpleDateFormat(format);
                try {
                    Date inputDate = dateFormat.parse(inputString);
                    String birthDay = new SimpleDateFormat("dd-MM-yyyy").format(inputDate);
                    mBirthDayEditText.setText(birthDay);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            mentorSwitch.setChecked(user.isMentor());
        }

    }

    public Map<String, String> getInput() {
        Map<String, String> map = new HashMap<String, String>();
        String firstname = mFirstNameEditText.getText().toString().trim();
        String lastname = mLastNameEditText.getText().toString().trim();
        String phoneNumber = mPhoneNumberEditText.getText().toString().trim();
        String isMentor = (mentorSwitch.isChecked() ? "true" : "false");
        String birthday = mBirthDayEditText.getText().toString();
        String gender = (mGenderEditText.getText().toString().equalsIgnoreCase("male") ? "m" : "f");
        map.put(Constants.FIRST_NAME, firstname);
        map.put(Constants.LAST_NAME, lastname);
        map.put(Constants.PHONE_NUMBER, phoneNumber);
        map.put(Constants.BIRTH_DAY, birthday);
        map.put(Constants.GENDER, gender);
        map.put(Constants.IS_MENTOR, isMentor);
        return map;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.birthday:
                if (b) {
                    showDatePickerDialog(view);
                }
                break;
            case R.id.gender:
                if (b) {
                    showGenderPopup(getContext(), view);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_bio_button:
                if (validateFields(new EditText[]{mFirstNameEditText, mLastNameEditText, mPhoneNumberEditText, mGenderEditText, mBirthDayEditText})) {
                    Bundle bundle = new Bundle();
                    basicProfileData = getInput();
                    bundle.putSerializable(Constants.BASIC_PROFILE_DATA, (Serializable) basicProfileData);
                    bundle.putParcelable(Constants.USER, user);

                    if (mentorSwitch.isChecked()) {
                        startFragment(MentorInterestsFragment.newInstance(bundle));
                    } else {
                        startFragment(BioProfileFragment.newInstance(bundle));
                    }
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.validation_prompt_label));
                }
                break;
            case R.id.birthday:
                showDatePickerDialog(view);
                break;
            case R.id.gender:
                showGenderPopup(getContext(), view);
                break;
        }
    }

    public void showGenderPopup(final Context context, View view) {
        AlertDialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        CharSequence options[];
        String title = null;
        options = new String[]{"Male", "Female"};
        title = "Select your gender";

        builder.setTitle(title);
        final CharSequence[] finalOptions = options;
        builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        String selectedOption = finalOptions[position].toString();
                        mGenderEditText.setText(selectedOption);
                    }
                }
        );
        dialog = builder.create();
        dialog.show();
    }

    public void showDatePickerDialog(View v) {
        final Calendar c = GregorianCalendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Calendar maxDate = GregorianCalendar.getInstance();
        maxDate.set(Calendar.YEAR, year - 5);
        maxDate.set(Calendar.MONTH, 11);
        maxDate.set(Calendar.DAY_OF_MONTH, 31);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        datePickerDialog.show();
    }

    private void fetchUserProfile(String emailAddress) {
        if (emailAddress != null && emailAddress.length() > 0) {
            if (NetworkUtils.isConnected(getContext())) {
                new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_PROFILE_REQUEST, emailAddress, new ApiClientListener.FetchUserProfileListener() {
                    @Override
                    public void onProfileFetched(UserProfileResponse userProfileResponse) {
                        if (userProfileResponse != null && userProfileResponse.getMessage() == null) {
                            user = extractUserDetails(userProfileResponse);
                            PreferenceStorageManager.saveUser(WelcomeMatApplication.getAppInstance().getApplicationContext(), user);
                            loadUserData(user);
                        }
                    }
                }).execute();
            }
        } else {
            closeFragment();
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(year, month, day);
        String birthDay = DateUtils.getSimpleDateFormat(calendar.getTimeInMillis(), "dd-MM-yyyy");
        mBirthDayEditText.setText(birthDay);
    }
}
