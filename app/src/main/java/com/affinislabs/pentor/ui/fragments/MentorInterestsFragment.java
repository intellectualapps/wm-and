package com.affinislabs.pentor.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.InterestResponse;
import com.affinislabs.pentor.api.responses.SubInterestResponse;
import com.affinislabs.pentor.models.Interest;
import com.affinislabs.pentor.models.SubInterest;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.MentorActivity;
import com.affinislabs.pentor.ui.activities.ProfileActivity;
import com.affinislabs.pentor.ui.adapters.InterestAdapter;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.ListUtils;
import com.affinislabs.pentor.utils.NetworkUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MentorInterestsFragment extends BaseFragment implements View.OnClickListener, InterestAdapter.ViewHolder.ClickListener {
    private static final String TAG = MentorInterestsFragment.class.getSimpleName();
    private Toolbar toolbar;
    private View mSnackBarView;
    private User user;
    private TextView mShowBioView;
    private ArrayList<Interest> interests;
    private ArrayList<SubInterest> subInterests;
    private RecyclerView mInterestsRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private InterestAdapter mAdapter;
    private Map<String, String> subInterestMap;
    private ArrayList<String> mSelectedSubInterests;
    private Map<String, String> basicProfileData;
    private String selectedSubInterest;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new MentorInterestsFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public MentorInterestsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }

            if (getArguments().containsKey(Constants.BASIC_PROFILE_DATA)) {
                basicProfileData = (Map<String, String>) getArguments().getSerializable(Constants.BASIC_PROFILE_DATA);
            }
        }

        mSelectedSubInterests = new ArrayList<String>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mentor_interests, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private Toolbar initializeToolbar(Activity activity) {
        if (isMentorActivityInstance(activity)) {
            return ((MentorActivity) getActivity()).getToolbar();
        } else {
            return ((ProfileActivity) getActivity()).getToolbar();
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = initializeToolbar(getActivity());
        toolbar.setTitle(getString(R.string.interests_view_label));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMentorActivityInstance(getActivity())) {
                    ((MentorActivity) getActivity()).navigateUp();
                } else {
                    ((ProfileActivity) getActivity()).navigateUp();
                }
            }
        });
        mShowBioView = (TextView) view.findViewById(R.id.show_bio_button);
        mInterestsRecyclerView = (RecyclerView) view.findViewById(R.id.interests_recyclerview);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        mAdapter = new InterestAdapter(getContext(), this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
        mInterestsRecyclerView.setLayoutManager(mLayoutManager);
        mInterestsRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout.setColorSchemeColors(
                getActivity().getResources().getColor(R.color.orange),
                getActivity().getResources().getColor(R.color.green),
                getActivity().getResources().getColor(R.color.blue)
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    fetchInterests();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        loadUserInterests(user);

        mShowBioView.setOnClickListener(this);
        fetchInterests();

        if (isMentorActivityInstance(getActivity())) {
            mShowBioView.setText(getString(R.string.select_category_prompt));
            mShowBioView.setEnabled(false);
        } else {
            mShowBioView.setText(getString(R.string.next_label));
            mShowBioView.setEnabled(true);
        }
    }

    private void loadUserInterests(User user) {
        if (user != null && user.getMentorInterests() != null && user.getMentorInterests().size() > 0) {
            mSelectedSubInterests.clear();
            addToMap(user.getMentorInterests());
            for (SubInterest subInterest : user.getMentorInterests()) {
                mSelectedSubInterests.add(subInterest.getName());
            }
        }
    }

    private boolean isMentorActivityInstance(Activity activity) {
        return activity instanceof MentorActivity;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_bio_button:
                if (mSelectedSubInterests != null && mSelectedSubInterests.size() > 0) {
                    Bundle bundle = new Bundle();
                    if (isMentorActivityInstance(getActivity())) {
                        bundle.putString(Constants.MENTOR_INTERESTS, getSelectedSubInterestIds());
                        bundle.putParcelable(Constants.USER, user);
                        startFragment(MentorListFragment.newInstance(bundle));
                    } else {
                        basicProfileData.put(Constants.MENTOR_INTERESTS, getSelectedSubInterestIds());
                        bundle.putSerializable(Constants.BASIC_PROFILE_DATA, (Serializable) basicProfileData);
                        bundle.putParcelable(Constants.USER, user);
                        startFragment(BioProfileFragment.newInstance(bundle));
                    }
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.interests_empty_error));
                }
                break;
        }
    }

    private void fetchInterests() {
        final String message = getString(R.string.fetch_interests);
        if (NetworkUtils.isConnected(getContext())) {
            showLoadingIndicator(true, message);
            new ApiClient.NetworkCallsRunner(Constants.FETCH_INTERESTS_REQUEST, new ApiClientListener.FetchInterestsListener() {
                @Override
                public void onInterestsFetched(InterestResponse interestResponse) {
                    showLoadingIndicator(false, message);
                    if (mSwipeRefreshLayout.isRefreshing())
                        mSwipeRefreshLayout.setRefreshing(false);

                    if (interestResponse != null && interestResponse.getMessage() == null) {
                        interests = interestResponse.getInterests();
                        mAdapter.setItems(interests);
                    }
                }
            }).execute();
        } else {
            CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
        }
    }

    private void fetchSubInterests(String interestId) {
        final String message = getString(R.string.fetch_subinterests);
        if (NetworkUtils.isConnected(getContext())) {
            showLoadingIndicator(true, message);
            new ApiClient.NetworkCallsRunner(Constants.FETCH_SUB_INTERESTS_REQUEST, interestId, new ApiClientListener.FetchSubInterestsListener() {
                @Override
                public void onSubInterestsFetched(SubInterestResponse subInterestResponse) {
                    showLoadingIndicator(false, message);
                    if (subInterestResponse != null && subInterestResponse.getMessage() == null) {
                        subInterests = subInterestResponse.getSubInterests();
                        addToMap(subInterests);
                        showSubInterestPopup(getContext(), ListUtils.getSubInterests(subInterests));
                    }
                }
            }).execute();
        } else {
            CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
        }
    }

    public void showSubInterestPopup(final Context context, final String[] subInterests) {
        AlertDialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
        String title = "Sub-categories";
        final ArrayList<String> selectedItems = new ArrayList<String>();
        final ArrayList<String> deSelectedItems = new ArrayList<String>();
        final boolean[] checkedItems = new boolean[subInterests.length];
        for (int i = 0; i < subInterests.length; i++) {
            checkedItems[i] = mSelectedSubInterests != null && mSelectedSubInterests.size() > 0 && mSelectedSubInterests.contains(subInterests[i]);
        }

        builder.setTitle(title);
        if (isMentorActivityInstance(getActivity())) {
            builder.setSingleChoiceItems(subInterests, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    selectedSubInterest = subInterestMap.get(subInterests[whichButton]);
                }
            });
        } else {
            builder.setMultiChoiceItems(subInterests, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    if (isChecked) {
                        selectedItems.add(subInterests[which]);
                        deSelectedItems.remove(subInterests[which]);
                    } else {
                        selectedItems.remove(subInterests[which]);
                        deSelectedItems.add(subInterests[which]);
                    }
                }
            });
        }

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isMentorActivityInstance(getActivity())) {
                    dialog.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.USER, user);
                    bundle.putString(Constants.MENTOR_INTERESTS, selectedSubInterest);
                    startFragment(MentorListFragment.newInstance(bundle));
                } else {
                    resolveInterests(selectedItems, deSelectedItems);
                    dialog.dismiss();
                }
            }
        }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    private void addToMap(ArrayList<SubInterest> items) {
        if (subInterestMap == null)
            subInterestMap = new HashMap<String, String>();

        for (SubInterest subInterest : items) {
            if (!subInterestMap.containsKey(subInterest.getName())) {
                subInterestMap.put(subInterest.getName(), subInterest.getSubId());
            }
        }
    }

    private String getSelectedSubInterestIds() {
        ArrayList<String> selectedItemIds = new ArrayList<String>();
        for (String subInterestName : mSelectedSubInterests) {
            String subInterestId = subInterestMap.get(subInterestName);
            if (!selectedItemIds.contains(subInterestId))
                selectedItemIds.add(subInterestId);
        }
        return android.text.TextUtils.join(",", selectedItemIds);
    }

    private void resolveInterests(ArrayList<String> items, ArrayList<String> deselectedItems) {
        if (mSelectedSubInterests == null) {
            mSelectedSubInterests = new ArrayList<String>();
        }

        if (items != null && items.size() > 0) {
            for (String item : items) {
                if (!mSelectedSubInterests.contains(item)) {
                    mSelectedSubInterests.add(item);
                }
            }
        }

        if (deselectedItems != null && deselectedItems.size() > 0) {
            for (String item : deselectedItems) {
                if (mSelectedSubInterests.contains(item)) {
                    mSelectedSubInterests.remove(item);
                }
            }
        }
    }

    @Override
    public void onItemClicked(int position) {
        Interest interest = interests.get(position);
        String interestId = interest.getId();
        fetchSubInterests(interestId);
    }
}
