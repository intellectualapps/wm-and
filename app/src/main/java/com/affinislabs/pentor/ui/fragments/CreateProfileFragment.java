package com.affinislabs.pentor.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.ui.activities.ProfileActivity;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.utils.Constants;

public class CreateProfileFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = CreateProfileFragment.class.getSimpleName();
    Toolbar toolbar;
    TextView mCreateProfileBtn;
    ImageView mCreateProfileIcon;
    View mSnackBarAnchor;
    static User user;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new CreateProfileFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public CreateProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_profile, container, false);
        mSnackBarAnchor = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((HomeActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.home_label));
        mCreateProfileBtn = (TextView) view.findViewById(R.id.create_profile_button);
        mCreateProfileIcon = (ImageView) view.findViewById(R.id.create_profile);

        mCreateProfileBtn.setOnClickListener(this);
        mCreateProfileIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_profile:
            case R.id.create_profile_button:
                Intent intent = new Intent(getContext(), ProfileActivity.class);
                intent.putExtra(Constants.VIEW_TYPE, Constants.BASIC_PROFILE_VIEW_TAG);
                intent.putExtra(Constants.USER, user);
                startActivity(intent);
                break;
        }
    }
}
