package com.affinislabs.pentor.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.dialogs.MediaPickerDialog;
import com.affinislabs.pentor.ui.fragments.ActiveConnectionFragment;
import com.affinislabs.pentor.ui.fragments.MediaPickerBaseFragment;
import com.affinislabs.pentor.utils.Constants;

import java.util.Map;

public class ChatActivity extends BaseActivity implements MediaPickerDialog.MediaPickedListener {
    Bundle fragmentBundle;
    String viewType;
    User user;
    Connection connection;
    Toolbar toolbar;
    Map<String, String> basicProfileData;
    public static int CLOSE_REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.chats));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            fragmentBundle = intent.getExtras();
            viewType = intent.getExtras().getString(Constants.VIEW_TYPE);
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    default:
                    case Constants.ACTIVE_CONNECTION_VIEW_TAG:
                        frag = ActiveConnectionFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = ActiveConnectionFragment.newInstance(fragmentBundle);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }

    public Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
        }
        return toolbar;
    }

    public void navigateUp() {
        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if (backstack > 0) {
            //just pop
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onMediaItemPicked(Uri fileUri, int mediaType, String fragmentTag) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        if (frag != null) {
            //check if the fragment is an instance of MediaPickerBaseFragment
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerSuccess(fileUri, mediaType, fragmentTag);
            }
        }
    }

    @Override
    public void onCancel(String message, String fragmentTag) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        if (frag != null) {
            //check if the fragment is an instance of MediaPickerBaseFragment
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerError(message, fragmentTag);
            }
        }
    }
}
