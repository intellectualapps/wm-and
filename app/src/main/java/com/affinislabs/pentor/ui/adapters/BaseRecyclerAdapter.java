package com.affinislabs.pentor.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.affinislabs.pentor.R;

public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {
    static Context context;
    LayoutInflater mInflater;

    public BaseRecyclerAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public OnItemClickListener mItemClickListener;
    public ClickListener mClickListener;

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    public interface OnItemClickListener {
        public void onClick(int position, View view);
    }

    public interface ClickListener {
        /**
         * Called when the view is clicked.
         *
         * @param v           view that is clicked
         * @param position    of the clicked item
         * @param isLongClick true if long click, false otherwise
         */
        public void onClick(View v, int position, boolean isLongClick);
    }

    /* Setter for listener. */
    public void setClickListener(ClickListener clickListener) {
        this.mClickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        // If not long clicked, pass last variable as false.
        //clickListener.onClick(v, getPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {
        // If long clicked, passed last variable as true.
        //clickListener.onClick(v, getPosition(), true);
        return true;
    }

    public static void startFragment(Fragment fragment, boolean addToBackStack, FragmentManager fragmentManager) {
        try {
            if (fragment != null) {
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
                if (addToBackStack)
                    ft.addToBackStack(null);
                ft.commitAllowingStateLoss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startFragment(Fragment frag, Activity activity) {
        if (activity != null && frag != null) {
            FragmentTransaction ft = ((AppCompatActivity) activity).getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

}
