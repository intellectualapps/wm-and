package com.affinislabs.pentor.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.ConnectionsActivity;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.NetworkUtils;

public class MentorRequestFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = MentorRequestFragment.class.getSimpleName();
    private Toolbar toolbar;
    private TextView mMentorNameView;
    private TextView mMentorShortBioView;
    private TextView mRequestStatusView;
    private ImageView mMentorProfilePhoto;

    private View mSnackBarAnchor;
    private User mentor, mentee;
    private Connection connection;
    private String message;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new MentorRequestFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public MentorRequestFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.MENTOR_KEY)) {
                mentor = getArguments().getParcelable(Constants.MENTOR_KEY);
            }

            if (getArguments().containsKey(Constants.MENTEE_KEY)) {
                mentee = getArguments().getParcelable(Constants.MENTEE_KEY);
            }

            if (getArguments().containsKey(Constants.CONNECTION)) {
                connection = getArguments().getParcelable(Constants.CONNECTION);
            }
        }
        message = getString(R.string.fetching_profile_progress);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mentor_request, container, false);
        mSnackBarAnchor = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((ConnectionsActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.connections));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ConnectionsActivity) getActivity()).navigateUp();
            }
        });

        mMentorNameView = (TextView) view.findViewById(R.id.mentor_name);
        mMentorShortBioView = (TextView) view.findViewById(R.id.mentor_short_bio);
        mRequestStatusView = (TextView) view.findViewById(R.id.request_status_view);
        mMentorProfilePhoto = (ImageView) view.findViewById(R.id.mentor_profile_photo);

        loadMentorData(mentor, connection);
        fetchUserProfile(mentor.getEmail());
    }

    private void loadMentorData(User user, Connection connection) {
        if (user != null) {
            mMentorNameView.setText(user.getFirstName().concat(" " + user.getLastName()));
            mMentorShortBioView.setText(user.getShortBio());
            mRequestStatusView.setText(connection.getCurrentStatus());

            if (user.getProfilePhotoUrl() != null) {
                if (user.getProfilePhotoUrl().contains(getString(R.string.base_url))) {
                    String imageKey = user.getProfilePhotoUrl().split("image/")[1];
                    if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                        loadImageData(imageKey, mMentorProfilePhoto);
                    } else {
                        fetchImageBackgroundTask(imageKey, mMentorProfilePhoto, getContext());
                    }
                } else {
                    GlideUtils.loadGlideImage(mMentorProfilePhoto, getContext(), user.getProfilePhotoUrl());
                }
            } else {
                GlideUtils.loadGlideImage(mMentorProfilePhoto, getContext(), R.drawable.ic_mentor);
            }

        } else {
            closeFragment();
        }
    }

    private void fetchUserProfile(String emailAddress) {
        if (emailAddress != null && emailAddress.length() > 0) {
            if (NetworkUtils.isConnected(getContext())) {
                showLoadingIndicator(true, message);
                new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_PROFILE_REQUEST, emailAddress, new ApiClientListener.FetchUserProfileListener() {
                    @Override
                    public void onProfileFetched(UserProfileResponse userProfileResponse) {
                        showLoadingIndicator(false, message);
                        if (userProfileResponse != null && userProfileResponse.getMessage() == null) {
                            User user = extractUserDetails(userProfileResponse);
                            loadMentorData(user, connection);
                        }
                    }
                }).execute();
            } else {
                CommonUtils.displaySnackBarMessage(mSnackBarAnchor, getString(R.string.network_connection_error));
            }
        } else {
            closeFragment();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }
}
