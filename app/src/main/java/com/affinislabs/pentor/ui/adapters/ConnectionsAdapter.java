package com.affinislabs.pentor.ui.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.pentor.WelcomeMatApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.ListUtils;
import com.affinislabs.pentor.utils.NetworkUtils;

import java.util.ArrayList;

public class ConnectionsAdapter extends BaseRecyclerAdapter {
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mConnectionProfilePhoto;
        TextView mConnectionName;
        View container;
        private ClickListener clickListener;

        public ViewHolder(View view, ViewHolder.ClickListener clickListener) {
            super(view);
            mConnectionName = (TextView) view.findViewById(R.id.mentor_name);
            mConnectionProfilePhoto = (ImageView) view.findViewById(R.id.mentor_profile_photo);
            container = view.findViewById(R.id.container);
            this.clickListener = clickListener;
            view.setOnClickListener(this);
            mConnectionName.setOnClickListener(this);
            mConnectionProfilePhoto.setOnClickListener(this);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClicked(getAdapterPosition());
            }
        }

        public interface ClickListener {
            void onItemClicked(int position);
        }
    }

    private ArrayList<Connection> mConnections;
    ViewHolder.ClickListener clickListener;

    public ConnectionsAdapter(Context context, ViewHolder.ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    public void setItems(ArrayList<Connection> connections) {
        this.mConnections = connections;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (this.mConnections != null)
            mConnections.clear();
        notifyDataSetChanged();
    }

    public void addItem(Connection connection) {
        if (connection != null) {
            this.mConnections.add(connection);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_connection_item, parent, false);

        return new ViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final Connection connection = mConnections.get(position);
        User user = null;

        if (connection.getMentee() != null) {
            user = connection.getMentee();
        }

        if (connection.getMentor() != null) {
            user = connection.getMentor();
        }

        final ViewHolder holder = (ViewHolder) viewHolder;
        if (user != null) {
            holder.mConnectionName.setText(user.getFirstName().concat(" " + user.getLastName()));
            if (user.getProfilePhotoUrl() != null) {
                if (user.getProfilePhotoUrl().contains(context.getString(R.string.base_url))) {
                    String imageKey = user.getProfilePhotoUrl().split("image/")[1];

                    if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                        String cachedValue = FileUtils.getImageDataFromCache(imageKey);
                        try {
                            byte[] decodedImageResource = Base64.decode(cachedValue, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                            holder.mConnectionProfilePhoto.setImageBitmap(decodedByte);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        fetchImageBackgroundTask(imageKey, holder.mConnectionProfilePhoto);
                    }
                } else {
                    GlideUtils.loadGlideImage(holder.mConnectionProfilePhoto, WelcomeMatApplication.getAppInstance().getApplicationContext(), user.getProfilePhotoUrl());
                }
            } else {
                holder.mConnectionProfilePhoto.setImageDrawable(null);
            }
        } else {
            holder.itemView.setVisibility(View.GONE);
        }
    }

    private void fetchImageBackgroundTask(final String imageKey, final ImageView imageView) {
        if (NetworkUtils.isConnected(context)) {
            new ApiClient.NetworkCallsRunner(Constants.FETCH_IMAGE_REQUEST, imageKey, new ApiClientListener.FetchEncodedImageListener() {
                @Override
                public void onEncodedImageFetched(ImageResponse imageResponse) {
                    if (imageResponse != null && imageResponse.getBase64ImageString() != null && imageResponse.getBase64ImageString().length() > 0) {
                        FileUtils.saveImageToCache(imageKey, imageResponse.getBase64ImageString());
                        byte[] decodedImageResource = Base64.decode(imageResponse.getBase64ImageString(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                        imageView.setImageBitmap(decodedByte);
                    }
                }
            }).execute();
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(mConnections) ? 0 : mConnections.size();
    }
}
