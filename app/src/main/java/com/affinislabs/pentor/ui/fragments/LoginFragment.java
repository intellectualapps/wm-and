package com.affinislabs.pentor.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.affinislabs.pentor.WelcomeMatApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.ApiModule;
import com.affinislabs.pentor.api.responses.LoginResponse;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.ui.customviews.CustomButton;
import com.affinislabs.pentor.ui.customviews.CustomTwitterLoginButton;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.ui.dialogs.EndConnectionDialog;
import com.affinislabs.pentor.ui.dialogs.ResetPasswordDialog;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.affinislabs.pentor.utils.Validator;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

public class LoginFragment extends BaseFragment implements View.OnClickListener {
    Toolbar toolbar;
    TextView mLoginView, mSignupView, mResetPasswordView;
    EditText mEmailAddressEditText, mPasswordEditText;
    ToggleButton mTogglePasswordVisibility;
    CustomButton mFacebookLoginButton;
    CustomTwitterLoginButton mTwitterButton;
    CustomButton mLinkedInButton;

    User user;
    View mSnackBarView;

    String message, error;

    CallbackManager callbackManager;
    FacebookCallback<LoginResult> callback;
    Callback<TwitterSession> twitterSessionCallback;
    static TwitterAuthClient authClient;
    public static final String PACKAGE = "com.affinislabs.pentor";
    private static final String host = "api.linkedin.com";
    private static final String url = "https://" + host
            + "/v1/people/~:" +
            "(email-address,formatted-name,phone-numbers,picture-urls::(original))";


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new LoginFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authClient = new TwitterAuthClient();
        //generateHashkey();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        mLoginView = (TextView) view.findViewById(R.id.login_button);
        mSignupView = (TextView) view.findViewById(R.id.signup_button);
        mResetPasswordView = (TextView) view.findViewById(R.id.reset_password_button);
        mTogglePasswordVisibility = (ToggleButton) view.findViewById(R.id.toggle_password_visibility);
        mEmailAddressEditText = (EditText) view.findViewById(R.id.email_address);
        mPasswordEditText = (EditText) view.findViewById(R.id.password);
        mFacebookLoginButton = (CustomButton) view.findViewById(R.id.facebook_login_button);
        mLinkedInButton = (CustomButton) view.findViewById(R.id.linkedin_login_button);
        mTwitterButton = (CustomTwitterLoginButton) view.findViewById(R.id.twitter_login_button);

        mTwitterButton.setText(getString(R.string.twitter_login_label));
        mTogglePasswordVisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    mPasswordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    mPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }

                int pwdLength = mPasswordEditText.getText().toString().trim().length();
                mPasswordEditText.setSelection(pwdLength > 0 ? pwdLength : 0);
            }
        });

        mResetPasswordView.setPaintFlags(mResetPasswordView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        managerTwitterAuthentication();
        mSignupView.setOnClickListener(this);
        mResetPasswordView.setOnClickListener(this);
        mLoginView.setOnClickListener(this);
        mLinkedInButton.setOnClickListener(this);
        mFacebookLoginButton.setOnClickListener(this);
        loadSavedLoginData();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public boolean validateFields() {
        return Validator.validateInputViewsNotEmpty(new EditText[]{mEmailAddressEditText, mPasswordEditText});
    }

    private void loadSavedLoginData() {
        Map<String, String> loginData = new HashMap<String, String>();
        loginData = PreferenceStorageManager.getLoginData(getContext().getApplicationContext());
        if (loginData != null && loginData.size() > 0) {
            mEmailAddressEditText.setText(loginData.get(Constants.EMAIL_ADDRESS));
            mPasswordEditText.setText(loginData.get(Constants.PASSWORD));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        mTwitterButton.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getContext())
                .onActivityResult(getActivity(), requestCode, resultCode, data);
        if (data != null && data.getExtras() != null) {
            Log.w("ActivityResult", data.getExtras().toString());
        }
        if (data != null && data.getAction() != null && data.getAction().equalsIgnoreCase("com.linkedin.thirdparty.authorize.RESULT_ACTION")) {
            linkedInApiHelper();
        }
    }

    private class TwitterLoginHandler extends Callback<TwitterSession> {
        @Override
        public void success(final Result<TwitterSession> result) {
            message = getString(R.string.login_twitter_progress_label);
            showLoadingIndicator(true, message);
            final TwitterSession session = result.data;
            TwitterAuthToken authToken = session.getAuthToken();
            String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";
            TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(false, false)
                    .enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
                        @Override
                        public void success(Result<com.twitter.sdk.android.core.models.User> userResult) {
                            final Map<String, String> accountMap = new HashMap<String, String>();
                            String names[] = userResult.data.name.split(" ");
                            String firstName = null, lastName = null;
                            firstName = names[1];
                            lastName = names[0];
                            String profilePhoto = userResult.data.profileImageUrl.replace("_normal", "");

                            saveSocialProfileData(firstName, lastName, profilePhoto, null);

                            TwitterAuthClient authClient2 = new TwitterAuthClient();
                            authClient2.requestEmail(session, new Callback<String>() {
                                @Override
                                public void success(Result<String> result) {
                                    accountMap.put(Constants.EMAIL_ADDRESS, result.data);
                                    message = getString(R.string.login_twitter_progress_label);
                                    accountMap.put(Constants.PASSWORD, "na");
                                    showLoadingIndicator(true, message);
                                    makeAPICall(accountMap);
                                }

                                @Override
                                public void failure(TwitterException exception) {
                                    message = getString(R.string.registration_twitter_progress_label);
                                    error = getString(R.string.twitter_login_error);
                                    showLoadingIndicator(false, message);
                                    CommonUtils.displaySnackBarMessage(mSnackBarView, error);
                                    exception.printStackTrace();
                                }
                            });
                        }

                        @Override
                        public void failure(TwitterException exc) {
                            message = getString(R.string.registration_twitter_progress_label);
                            error = getString(R.string.twitter_login_error);
                            showLoadingIndicator(false, message);
                            CommonUtils.displaySnackBarMessage(mSnackBarView, error);
                            exc.printStackTrace();
                            Log.d("TwitterKit", "Verify Credentials Failure", exc);
                        }
                    });
        }

        @Override
        public void failure(TwitterException exception) {
            message = getString(R.string.registration_twitter_progress_label);
            error = getString(R.string.twitter_login_error);
            showLoadingIndicator(false, message);
            CommonUtils.displaySnackBarMessage(mSnackBarView, error);
            exception.printStackTrace();
        }
    }

    public void managerTwitterAuthentication() {
        mTwitterButton.setCallback(new TwitterLoginHandler());
    }

    public void generateHashkey() {
        try {
            PackageInfo info = getContext().getPackageManager().getPackageInfo(
                    PACKAGE,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.w("LinkedIn HashKey", Base64.encodeToString(md.digest(), Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("Name not found", e.getMessage(), e);

        } catch (NoSuchAlgorithmException e) {
            Log.d("Error", e.getMessage(), e);
        }
    }

    // set the permission to retrieve basic
    //information of User's linkedIn account
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    public void linkedInApiHelper() {
        APIHelper apiHelper = APIHelper.getInstance(getContext());
        apiHelper.getRequest(getContext(), url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse result) {
                try {
                    Log.w("LinkedIn Data", result.getResponseDataAsString());
                    Log.w("LinkedIn Data", result.getResponseDataAsJson().toString());
                    Toast.makeText(getContext(), result.getResponseDataAsJson().toString(), Toast.LENGTH_SHORT).show();
                    //progress.dismiss();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onApiError(LIApiError error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                if (validateFields(new EditText[]{mEmailAddressEditText, mPasswordEditText})) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        message = getString(R.string.login_progress_label);
                        showLoadingIndicator(true, message);
                        PreferenceStorageManager.setLoginData(getContext().getApplicationContext(), getUserInput().get(Constants.EMAIL_ADDRESS), getUserInput().get(Constants.PASSWORD));
                        makeAPICall(getUserInput());
                    } else {
                        CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                    }
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.validation_prompt_label));
                }
                break;
            case R.id.linkedin_login_button:
                if (NetworkUtils.isConnected(getContext())) {
                    LISessionManager.getInstance(getContext())
                            .init(getActivity(), buildScope(), new AuthListener() {
                                @Override
                                public void onAuthSuccess() {

                                    Toast.makeText(getContext(), "success" +
                                                    LISessionManager
                                                            .getInstance(getContext())
                                                            .getSession().getAccessToken().toString(),
                                            Toast.LENGTH_SHORT).show();

                                }

                                @Override
                                public void onAuthError(LIAuthError error) {

                                    Toast.makeText(getContext(), "failed "
                                                    + error.toString(),
                                            Toast.LENGTH_LONG).show();
                                }
                            }, true);
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                }
                break;
            case R.id.facebook_login_button:
                if (NetworkUtils.isConnected(getContext())) {
                    manageFacebookAuthentication();
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                }
                break;
            case R.id.signup_button:
                replaceFragment(AuthFragment.newInstance(new Bundle()));
                break;
            case R.id.reset_password_button:
                Bundle args = new Bundle();
                args.putString(Constants.EMAIL_ADDRESS, getUserInput().get(Constants.EMAIL_ADDRESS));
                DialogFragment resetPasswordDialog = ResetPasswordDialog.getInstance(args);
                if (getActivity() != null)
                    resetPasswordDialog.show(getActivity().getSupportFragmentManager(), resetPasswordDialog.getClass().getSimpleName());
                break;
        }
    }

    public void manageFacebookAuthentication() {
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        final String PERMISSION_EMAIL = "email";
        final String PERMISSION_PUBLIC_PROFILE = "public_profile";

        ArrayList<String> permissions = new ArrayList<String>();
        permissions.add(PERMISSION_EMAIL);
        permissions.add(PERMISSION_PUBLIC_PROFILE);
        callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult != null) {
                    final AccessToken accessToken = loginResult.getAccessToken();
                    if (accessToken != null) {
                        //check declined permissions
                        Set<String> declinedPermissions = accessToken.getDeclinedPermissions();
                        if (declinedPermissions.isEmpty()) {
                            GraphRequest req = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                                    if (jsonObject != null) {
                                        try {
                                            Map<String, String> userMap = new HashMap<String, String>();

                                            String fbId = "";
                                            String email = "";
                                            String gender = "";
                                            String name = "";
                                            String profilePhoto = "";

                                            if (jsonObject.has("id"))
                                                fbId = jsonObject.getString("id");
                                            if (jsonObject.has("email"))
                                                email = jsonObject.getString("email");
                                            if (jsonObject.has("gender"))
                                                gender = jsonObject.getString("gender");
                                            if (jsonObject.has("name"))
                                                name = jsonObject.getString("name");
                                            if (jsonObject.has("picture"))
                                                profilePhoto = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");


                                            String[] split_names = name.split(" ");

                                            String firstName = split_names[0];
                                            String lastName = split_names[1];
                                            String gender_short = null;
                                            if (gender != null) {
                                                if (gender.equalsIgnoreCase("male")) {
                                                    gender_short = "m";
                                                } else if (gender.equalsIgnoreCase("female")) {
                                                    gender_short = "f";
                                                }
                                            }

                                            saveSocialProfileData(firstName, lastName, profilePhoto, gender_short);

                                            userMap.put(Constants.EMAIL_ADDRESS, email);
                                            userMap.put(Constants.PASSWORD, "na");
                                            message = getString(R.string.login_facebook_progress_label);
                                            showLoadingIndicator(true, message);
                                            makeAPICall(userMap);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                            String fields = "id,email,gender,name,picture.type(large)";
                            Bundle params = new Bundle();
                            params.putString("fields", fields);
                            req.setParameters(params);
                            req.executeAsync();
                        }
                    }
                } else {
                    Log.e("Facebook Login", "Null Login Result");
                    message = getString(R.string.login_facebook_progress_label);
                    error = getString(R.string.facebook_login_error);
                    showLoadingIndicator(false, message);
                    CommonUtils.displaySnackBarMessage(mSnackBarView, error);
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
                message = getString(R.string.login_facebook_progress_label);
                error = getString(R.string.facebook_login_error);
                showLoadingIndicator(false, message);
                CommonUtils.displaySnackBarMessage(mSnackBarView, error);
            }
        };
        LoginManager.getInstance().logInWithReadPermissions(this, permissions);
        LoginManager.getInstance().registerCallback(callbackManager, callback);
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String emailAddress = mEmailAddressEditText.getText().toString().trim();
        String password = mPasswordEditText.getText().toString().trim();
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.PASSWORD, password);
        return map;
    }

    private User extractUserDetails(LoginResponse loginResponse) {
        User user = new User();
        user.setAuthToken(loginResponse.getAuthToken());
        user.setEmail(loginResponse.getEmail());
        user.setFirstName(loginResponse.getFirstName());
        user.setLastName(loginResponse.getLastName());
        user.setProfilePhotoUrl(loginResponse.getProfilePhotoUrl());
        user.setShortBio(loginResponse.getShortBio());
        user.setLongBio(loginResponse.getLongBio());
        user.setEmailAddressVerified(loginResponse.isEmailAddressVerified());
        user.setPhoneNumber(loginResponse.getPhoneNumber());
        user.setGender(loginResponse.getGender());
        user.setBirthday(loginResponse.getBirthday());
        user.setMentor(loginResponse.isMentor());
        user.setLongitude(loginResponse.getLongitude());
        user.setLatitude(loginResponse.getLatitude());
        user.setAddress(loginResponse.getAddress());
        user.setUserRoles(loginResponse.getUserRoles());
        user.setMentorInterests(loginResponse.getMentorInterests());
        return user;
    }

    private void makeAPICall(Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.LOGIN_REQUEST, accountMap, new ApiClientListener.AccountAuthenticationListener() {
            @Override
            public void onUserAuthenticated(LoginResponse loginResponse) {
                showLoadingIndicator(false, message);
                if (loginResponse != null) {
                    if (loginResponse.getStatus() == null && loginResponse.getMessage() == null) {
                        User user = extractUserDetails(loginResponse);
                        //Reset ApiClient so the the authToken can be included in subsequent calls
                        ApiClient.NetworkCallsRunner.resetApiService();
                        ApiModule.resetApiClient();


                        showMainActivity(user);
                        PreferenceStorageManager.setLoggedInStatus(WelcomeMatApplication.getAppInstance().getApplicationContext(), true);
                    } else {
                        CommonUtils.displaySnackBarMessage(mSnackBarView, loginResponse.getMessage());
                    }
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void showMainActivity(User user) {
        Intent intent = new Intent(getContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Constants.USER, user);
        PreferenceStorageManager.saveUser(WelcomeMatApplication.getAppInstance().getApplicationContext(), user);
        PreferenceStorageManager.setSignInStatus(WelcomeMatApplication.getAppInstance().getApplicationContext(), true);
        PreferenceStorageManager.saveAuthToken(WelcomeMatApplication.getAppInstance().getApplicationContext(), user.getAuthToken());

        Map<String, String> metaDataMap = new HashMap<String, String>();
        metaDataMap.put(Constants.FCM_TOKEN, PreferenceStorageManager.getFCMToken(WelcomeMatApplication.getAppInstance().getApplicationContext()));
        metaDataMap.put(Constants.DEVICE_ID, PreferenceStorageManager.getDeviceId(WelcomeMatApplication.getAppInstance().getApplicationContext()));
        metaDataMap.put(Constants.DEVICE_PLATFORM_KEY, Constants.DEVICE_PLATFORM);
        uploadFCMToken(user.getEmail(), metaDataMap);

        startActivity(intent);
        getActivity().finish();
    }
}
