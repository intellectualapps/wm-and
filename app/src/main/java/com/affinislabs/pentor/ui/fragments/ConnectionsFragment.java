package com.affinislabs.pentor.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.affinislabs.pentor.WelcomeMatApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.ConnectionResponse;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.ChatActivity;
import com.affinislabs.pentor.ui.activities.ConnectionsActivity;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.ui.activities.MentorActivity;
import com.affinislabs.pentor.ui.adapters.ConnectionsAdapter;
import com.affinislabs.pentor.ui.customviews.CustomRecyclerView;
import com.affinislabs.pentor.ui.customviews.DividerItemDecoration;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;

import java.util.ArrayList;

public class ConnectionsFragment extends BaseFragment implements ConnectionsAdapter.ViewHolder.ClickListener, View.OnClickListener, PentorInterfaces.RefreshConnectionsListener {
    private Toolbar toolbar;
    private View mSnackBarView;
    private User user;
    private ConnectionsAdapter mAdapter;
    private CustomRecyclerView connectionsRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView mFindMentorView, mMentorPhotoLabel;
    private ImageView mMentorPhoto;
    private View mEmptyView;
    private String emailAddress;
    private ArrayList<Connection> mConnections;
    public static PentorInterfaces.RefreshConnectionsListener refreshConnectionsListener;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ConnectionsFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public ConnectionsFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
                emailAddress = user != null ? user.getEmail() : null;
            }
        }

        if ((emailAddress != null ? emailAddress.trim().length() : 0) < 1) {
            user = PreferenceStorageManager.getUser(WelcomeMatApplication.getAppInstance().getApplicationContext());
            emailAddress = user.getEmail();
        }

        mConnections = PreferenceStorageManager.getConnections(WelcomeMatApplication.getAppInstance().getApplicationContext());
        refreshConnectionsListener = this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connections, container, false);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((HomeActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.mentorships));

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mEmptyView = view.findViewById(R.id.empty_view);
        connectionsRecyclerView = (CustomRecyclerView) view.findViewById(R.id.connections_recyclerview);

        mFindMentorView = (TextView) view.findViewById(R.id.find_mentor);
        mMentorPhotoLabel = (TextView) view.findViewById(R.id.mentor_photo_label);
        mMentorPhoto = (ImageView) view.findViewById(R.id.mentor_placeholder_photo);

        mFindMentorView.setOnClickListener(this);
        mMentorPhotoLabel.setOnClickListener(this);
        mMentorPhoto.setOnClickListener(this);

        mAdapter = new ConnectionsAdapter(getContext(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        connectionsRecyclerView.setLayoutManager(mLayoutManager);
        connectionsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        connectionsRecyclerView.setAdapter(mAdapter);
        connectionsRecyclerView.setEmptyView(mEmptyView);
        connectionsRecyclerView.hasFixedSize();

        mSwipeRefreshLayout.setColorSchemeColors(
                getActivity().getResources().getColor(R.color.orange),
                getActivity().getResources().getColor(R.color.green),
                getActivity().getResources().getColor(R.color.blue)
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    fetchConnections(emailAddress, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        if (mConnections != null && mConnections.size() > 0) {
            mAdapter.setItems(mConnections);
            fetchConnections(emailAddress, false);
        } else {
            initializeData();
        }
    }

    void initializeData() {
        animateSwipeRefreshLayout(mSwipeRefreshLayout);
        fetchConnections(emailAddress, true);
    }

    private void fetchConnections(String emailAddress, final boolean displayLoader) {
        final String message = WelcomeMatApplication.getApplicationResources().getString(R.string.fetch_connections_loading_message);
        if (NetworkUtils.isConnected(getContext())) {
            if (displayLoader)
                showLoadingIndicator(true, message);
            new ApiClient.NetworkCallsRunner(Constants.FETCH_CONNECTIONS_REQUEST, emailAddress, new ApiClientListener.FetchConnectionsListener() {
                @Override
                public void onConnectionsFetched(ConnectionResponse connectionResponse) {
                    if (displayLoader)
                        showLoadingIndicator(false, message);
                    if (mSwipeRefreshLayout.isRefreshing())
                        mSwipeRefreshLayout.setRefreshing(false);

                    if (connectionResponse != null && connectionResponse.getMessage() == null) {
                        if (mConnections != null && mConnections.size() > 0)
                            mConnections.clear();
                        mConnections = new ArrayList<Connection>();
                        mConnections.addAll(connectionResponse.getMentees());
                        mConnections.addAll(connectionResponse.getMentors());
                        mAdapter.setItems(mConnections);
                        PreferenceStorageManager.saveConnections(WelcomeMatApplication.getAppInstance().getApplicationContext(), mConnections);
                    }
                }
            }).execute();
        } else {
            if (mSwipeRefreshLayout.isRefreshing())
                mSwipeRefreshLayout.setRefreshing(false);
            CommonUtils.displaySnackBarMessage(mSnackBarView, WelcomeMatApplication.getApplicationResources().getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mentor_placeholder_photo:
            case R.id.mentor_photo_label:
            case R.id.find_mentor:
                Intent intent = new Intent(getContext(), MentorActivity.class);
                intent.putExtra(Constants.VIEW_TYPE, Constants.MENTOR_INTERESTS_VIEW_TAG);
                intent.putExtra(Constants.USER, user);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getContext(), ConnectionsActivity.class);
        Connection connection = mConnections.get(position);
        String connectionStatus = connection.getCurrentStatus();
        String connectionId = connection.getConnectionId();
        intent.putExtra(Constants.CONNECTION, connection);

        switch (connectionStatus) {
            case Constants.ACTIVE_STATUS:
                intent = new Intent(getContext(), ChatActivity.class);
                intent.putExtra(Constants.VIEW_TYPE, Constants.ACTIVE_CONNECTION_VIEW_TAG);
                break;
            case Constants.PENDING_STATUS:
            case Constants.ADMIN_TERMINATED_STATUS:
            case Constants.DECLINED_STATUS:
            default:

                if (connection.getMentee() != null) {
                    intent.putExtra(Constants.VIEW_TYPE, Constants.MENTEE_REQUEST_VIEW_TAG);
                }

                if (connection.getMentor() != null) {
                    intent.putExtra(Constants.VIEW_TYPE, Constants.MENTOR_REQUEST_VIEW_TAG);
                }
                break;
        }

        if (connection.getMentee() != null) {
            intent.putExtra(Constants.MENTEE_KEY, connection.getMentee());
            intent.putExtra(Constants.MENTOR_KEY, user);
        }

        if (connection.getMentor() != null) {
            intent.putExtra(Constants.MENTOR_KEY, connection.getMentor());
            intent.putExtra(Constants.MENTEE_KEY, user);
        }

        intent.putExtra(Constants.CONNECTION_ID, connectionId);
        startActivity(intent);
    }

    @Override
    public void onConnectionRequestUpdated() {
        fetchConnections(emailAddress, false);
    }
}
