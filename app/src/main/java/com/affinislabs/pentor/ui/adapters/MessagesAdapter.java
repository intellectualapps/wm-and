package com.affinislabs.pentor.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.Message;
import com.affinislabs.pentor.ui.fragments.ActiveConnectionFragment;
import com.affinislabs.pentor.utils.ListUtils;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class MessagesAdapter extends BaseRecyclerAdapter {
    public static class ViewHolder extends RecyclerView.ViewHolder implements ClickListener, View.OnClickListener {
        TextView messageText;
        TextView messageTime;
        View container;
        private ClickListener clickListener;

        public ViewHolder(View view, ClickListener clickListener) {
            super(view);
            messageText = (TextView) view.findViewById(R.id.message_text);
            messageTime = (TextView) view.findViewById(R.id.message_time);
            container = view.findViewById(R.id.message_box);
            this.clickListener = clickListener;
            view.setOnClickListener(this);
            messageText.setOnClickListener(this);
            messageTime.setOnClickListener(this);
            container.setOnClickListener(this);
        }


        @Override
        public void onClick(View v, int position, boolean isLongClick) {
            if (clickListener != null) {
                clickListener.onClick(v, position, false);
            }
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onClick(v, getAdapterPosition(), false);
            }
        }
    }

    private ArrayList<Message> messages;
    private ClickListener clickListener;
    private final int SENT = 0, RECEIVED = 1, VISIBLE_THRESHOLD = 2;
    private String emailAddress;
    private PentorInterfaces.MessagePaginationListener messagePaginationListener;

    public MessagesAdapter(Context context, BaseRecyclerAdapter.ClickListener clickListener, PentorInterfaces.MessagePaginationListener messagePaginationListener, String emaiLAddress) {
        super(context);
        this.clickListener = clickListener;
        this.messagePaginationListener = messagePaginationListener;
        this.emailAddress = emaiLAddress;
    }

    public void setItems(ArrayList<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (this.messages != null)
            messages.clear();
        notifyDataSetChanged();
    }

    public void addItem(Message message) {
        if (message != null) {
            this.messages.add(message);
            notifyDataSetChanged();
        }
    }

    public interface EndlessScrollListener {
        /**
         * Loads more data.
         *
         * @param position
         * @return true loads data actually, false otherwise.
         */
        boolean onLoadMore(int position);
    }


    public interface EndlessTimelineScrollListener {
        /**
         * Loads more data.
         *
         * @param timestamp
         * @return true loads data actually, false otherwise.
         */
        boolean onLoadMore(int position, long timestamp);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = null;
        switch (viewType) {
            case RECEIVED:
                view = inflater.inflate(R.layout.layout_received_message, parent, false);
                break;
            default:
            case SENT:
                view = inflater.inflate(R.layout.layout_sent_message, parent, false);
                break;
        }
        viewHolder = new ViewHolder(view, clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final Message message = messages.get(position);

        final ViewHolder holder = (ViewHolder) viewHolder;
        if (message != null) {
            holder.messageText.setText(message.getText());

            long timestamp = message.getTime();
            String messageTimestamp = DateUtils.getRelativeTimeSpanString(timestamp, GregorianCalendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS).toString();
            if (messageTimestamp.contains("0 minutes")) {
                messageTimestamp = "just now";
            }
            holder.messageTime.setText(messageTimestamp);

            if (getItemCount() >= ActiveConnectionFragment.INITIAL_MESSAGE_LOAD_COUNT) {
                if (position == 0) {
                    if (messagePaginationListener != null) {
                        messagePaginationListener.onLoadMore(position, message.getId());
                    }
                }
            }

        } else {
            holder.itemView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messages.get(position);
        Boolean flag = message.getAuthor().equalsIgnoreCase(emailAddress);
        if (flag) {
            return SENT;
        } else {
            return RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(messages) ? 0 : messages.size();
    }
}
