package com.affinislabs.pentor.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import com.affinislabs.pentor.WelcomeMatApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FirebaseCloudMessagingListenerService extends FirebaseMessagingService {
    private static final String TAG = "FCM";
    public static NotificationManager mNotificationManager;
    NotificationCompat.Builder notificationBuilder;
    Handler handler;
    Map<String, String> bundle = new HashMap<>();
    String notificationCategory = null;
    User user = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> data = remoteMessage.getData();
            this.bundle = data;
            Log.w(TAG, data.toString());
            Context context = WelcomeMatApplication.getAppInstance().getApplicationContext();
            user = PreferenceStorageManager.getUser(context);
            if (user != null && PreferenceStorageManager.getSignInStatus(context)) {
                buildNotification(remoteMessage.getData());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchImageBackgroundTask(String imageKey, final NotificationCompat.Builder notificationBuilder, final Map<String, String> notificationData) {
        if (NetworkUtils.isConnected(getApplicationContext())) {
            new ApiClient.NetworkCallsRunner(Constants.FETCH_IMAGE_REQUEST, imageKey, new ApiClientListener.FetchEncodedImageListener() {
                @Override
                public void onEncodedImageFetched(ImageResponse imageResponse) {
                    if (imageResponse != null && imageResponse.getBase64ImageString() != null && imageResponse.getBase64ImageString().length() > 0) {
                        byte[] decodedImageResource = Base64.decode(imageResponse.getBase64ImageString(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                        displayNotification(decodedByte, notificationData, notificationBuilder);
                    } else {
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                        displayNotification(bitmap, notificationData, notificationBuilder);
                    }
                }
            }).execute();
        }
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void buildNotification(Map<String, String> notificationData) {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setAutoCancel(true)
                .setLights(Color.parseColor("green"), 5000, 1000)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{500, 500, 500, 500, 500});

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);

        String photoUrl = notificationData.get(Constants.PHOTO_URL);
        if (photoUrl != null && photoUrl.length() > 5) {
            if (photoUrl.contains(getString(R.string.base_url))) {
                String imageKey = photoUrl.split("image/")[1];
                if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                    String imageData = FileUtils.getImageDataFromCache(imageKey);
                    Bitmap decodedByte = null;
                    try {
                        byte[] decodedImageResource = Base64.decode(imageData, Base64.DEFAULT);
                        decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                    } catch (Exception e) {
                        e.printStackTrace();
                        decodedByte = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                    }
                    displayNotification(decodedByte, notificationData, notificationBuilder);

                } else {
                    fetchImageBackgroundTask(imageKey, notificationBuilder, notificationData);
                }
            } else {
                bitmap = getBitmapFromURL(photoUrl);
                displayNotification(bitmap, notificationData, notificationBuilder);
            }
        } else {
            displayNotification(bitmap, notificationData, notificationBuilder);
        }
    }

    private void displayNotification(Bitmap bitmap, Map<String, String> notificationData, NotificationCompat.Builder notificationBuilder) {
        Random rnd = new Random();
        int notificationId = 100000 + rnd.nextInt(900000);

        Intent intent = new Intent(this, HomeActivity.class);
        notificationBuilder.setLargeIcon(bitmap);
        NotificationCompat.Style notificationMessageStyle = new NotificationCompat.BigTextStyle()
                .bigText("Notification Big Text")
                .setBigContentTitle("Notification Big Content Title");
        //notificationBuilder.setStyle(notificationMessageStyle);

        String notificationType = notificationData.get(Constants.NOTIFICATION_TYPE);
        String connectionId = notificationData.get(Constants.CONNECTION_ID);
        switch (notificationType) {
            default:
            case Constants.CONNECTION_REQUEST:
                if (connectionId == null) {
                    return;
                }

                notificationBuilder.setContentTitle(Constants.CONNECTION_REQUEST_NOTIFICATION_TITLE);
                intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.MENTEE_REQUEST_VIEW_TAG);
                intent.putExtra(Constants.CONNECTION_ID, connectionId);
                break;
            case Constants.CONNECTION_ACCEPTED:
                notificationBuilder.setContentTitle(Constants.CONNECTION_REQUEST_NOTIFICATION_TITLE);
                intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.ACTIVE_CONNECTION_VIEW_TAG);
                intent.putExtra(Constants.CONNECTION_ID, connectionId);
                break;
            case Constants.CONNECTION_DECLINED:
                notificationBuilder.setContentTitle(Constants.CONNECTION_REQUEST_NOTIFICATION_TITLE);
                intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.CONNECTION_DECLINED_DIALOG);
                intent.putExtra(Constants.CONNECTION_ID, connectionId);
                break;
            case Constants.MESSAGE_NOTIFICATION:
                notificationBuilder.setContentTitle(Constants.MESSAGE_NOTIFICATION_TITLE);
                intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.ACTIVE_CONNECTION_VIEW_TAG);
                intent.putExtra(Constants.CONNECTION_ID, connectionId);
                break;
        }

        String notificationMessage = notificationData.get(Constants.NOTIFICATION_MESSAGE);
        notificationBuilder.setContentText(notificationMessage);

        intent.putExtra(Constants.NOTIFICATION_MESSAGE, notificationMessage);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Constants.NOTIFICATION_FLAG, true);

        User user = new User();
        user.setProfilePhotoUrl(notificationData.get(Constants.PHOTO_URL));
        user.setEmail(notificationData.get(Constants.MENTEE_EMAIL));
        intent.putExtra(Constants.NOTIFICATION_USER_PARAM, user);
        PendingIntent notificationPendingIntent =
                PendingIntent.getActivity(
                        this,
                        (int) System.currentTimeMillis(),
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notificationBuilder.setContentIntent(notificationPendingIntent);
        mNotificationManager.notify(notificationId, notificationBuilder.build());
    }
}
