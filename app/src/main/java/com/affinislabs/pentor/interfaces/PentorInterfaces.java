package com.affinislabs.pentor.interfaces;

import com.affinislabs.pentor.models.ConnectionNode;
import com.affinislabs.pentor.models.User;

public class PentorInterfaces {
    public interface MentorClickListener {
        void onMentorSkipClicked(User user, int currentIndex);

        void onMentorAddClicked(User user, int currentIndex);
    }

    public interface ChangeHomeActivityViewListener {
        void changeView(String viewTag);
    }

    public interface UserProfileUpdateListener {
        void onProfileUpdated(User user);
    }

    public interface RefreshConnectionsListener {
        void onConnectionRequestUpdated();
    }

    public interface FirebaseMessageListener {
        void onMessageSent(Boolean success);
    }

    public interface ConnectionDataListener{
        void onConnectionDataLoaded(ConnectionNode connectionNode);
    }

    public interface MessagePaginationListener {
        void onLoadMore(int position, String messageKey);
    }
}
